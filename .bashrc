# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# WE LIKE COLORS!
#force_color_prompt=yes

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# All alias definitions are going into the .bash_aliases file
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi


#####################
###   VARIABLES   ###
#####################

# Color code variables
RED="$(tput setaf 1)"
GRN="$(tput setaf 2)"
BLU="$(tput setaf 4)"
RST="$(tput sgr0)"
BOLD="$(tput bold)"


#####################
###   FUNCTIONS   ###
#####################

## Set the PS1 prompt 
prompt_command() {
  # First let's check to see which git branch we are in
  BRANCH=$(__git_ps1 "%s")
  PS1='\[$RED\]┌[\[$RST\]\u\[$RED\]@\[$RST\]\h\[$RED\]] '

  # If we are in a git repo, add the branch to the PS1 line and check to see
  # if changes need to be staged or committed to the branch
  if [ -n "$BRANCH" ]; then
    PS1+='\[$BLU\]\[$BOLD\]❬ \[$GRN\]${BRANCH}'
    if [ -n "$(git status -s)" ]; then
      PS1+='\[$RED\]🗴'
    fi
    PS1+='\[$BLU\]❭\[$RST\]'
  fi

  PS1+='\n\[$RED\]└[\[$RST\]\w\[$RED\]]>\[$RST\] '
}

PROMPT_COMMAND=prompt_command