#!/bin/bash

## Get all dotfiles from the current directory and set a path we will
## be using a lot
DOTFILES=$(find . -name '.*' -type f -printf '%f\n')
DOTFILEDIR="${HOME}/dotfiles"

## First, let's make the dotfiles directory exists in the home directory. If
## not, we'll create it and move all our stuffs into it
if [ -d "$DOTFILEDIR" ]
then
	echo "[ ] Dotfiles directory already exists. Proceeding..."
else
	echo "[*] Creating ${DOTFILEDIR} and moving files over"
	mkdir "$DOTFILEDIR"
	rsync -av --exclude=".git" --exclude="setup.sh" . "$DOTFILEDIR"
fi

## Create a backup folder for our old dotfiles
mkdir "${DOTFILEDIR}/backup"
echo "[ ] Created ~/dotfiles/backup"

## Backs up old dotfiles, then symlinks new ones in the user's home folder
for FILE in $DOTFILES
do
	if [ -f "${HOME}/${FILE}" ]
	then
		cp "${HOME}/${FILE}" "${DOTFILEDIR}/backup/${FILE}"
		echo "[ ] Copied original ${FILE} to ${DOTFILEDIR}/backup"
	fi
	## Uncomment below line to symlink the dotfiles
	ln -sf "${DOTFILEDIR}/${FILE}" "${HOME}/${FILE}"
	echo "[*] Symlinked ${FILE} to ${HOME}/${FILE}"
done

